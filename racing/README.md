
# C64 Style Racing Game

Old-school racing game written in Pascal, using the [SwinGame][1] library.

## Screenshot

![alt text](screenshot.png)

## Building

Before you build the game, you must install the SwinGame [back end library][2].

## Credits

* [C64 Style Racing Game][3] by [GGBot][6] 
* [The Essential Retro Video Game Sound Effects Collection][4] by Juhani Junkala
* [Croco Rocket][5] by Johan Brodd
* [Game Over Sound][7] by [den_yes][8]
* [Font Super Mario Bros 2][9] by Patrick Adams

[1]: https://github.com/macite/swingame
[2]: https://github.com/macite/swingame#building-the-back-end-library
[3]: https://opengameart.org/content/c64-style-racing-game
[4]: https://opengameart.org/content/512-sound-effects-8-bit-style
[5]: https://opengameart.org/content/crocorocket
[6]: https://opengameart.org/users/ggbot
[7]: https://opengameart.org/content/game-over-soundold-school
[8]: https://opengameart.org/users/denyes
[9]: https://www.dafont.com/super-mario-bros-2.font
