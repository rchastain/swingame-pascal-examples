# SwinGame Pascal examples

Pascal examples for the [SwinGame](https://github.com/macite/swingame) library.

## Contents

  * [Bear Animation Demo](bear)
  * [C64 Style Racing Game](racing)
  * [Josephus Problem](josephus)

## Screenshots

### Bear Animation Demo
![screenshot](bear/screenshot.png "Bear Animation Demo")
### C64 Style Racing Game
![screenshot](racing/screenshot.png "C64 Style Racing Game")
### Josephus Problem
![screenshot](josephus/screenshot.png "Josephus Problem")
