
# Josephus problem

Resolution of the Josephus problem, using the [SwinGame][1] library.

## Screenshot

![alt text](screenshot.png)

## Building

Before you build the game, you must install the SwinGame [back end library][2].

[1]: https://github.com/macite/swingame
