
# Animation demo

Animation demo using the [SwinGame][1] library.

## Screenshot

![alt text](screenshot.png)

## Building

Before you build the game, you must install the SwinGame [back end library][2].

## Credits

* [Animated Wild Animals][3] spritesheet
* [Lavinia][4] font

[1]: https://github.com/macite/swingame
[2]: https://github.com/macite/swingame#building-the-back-end-library
[3]: https://opengameart.org/content/animated-wild-animals
[4]: https://www.dafont.com/fr/lavinia.font
